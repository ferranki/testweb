var formService = {};

var nameForm = {};
nameForm.value = function () {
    return document.getElementById("name").value;
}

nameForm.isValid = function () {
    if (nameForm.value().replace(/\s/g, "") != "")
        return true;
    formService.SetError($("#name"));
    alert("Vaya..el campo Nombre no es correcto");
    return false;
}


var surnameForm = {};
surnameForm.value = function () {
    return document.getElementById("surname").value;
}

surnameForm.isValid = function () {
    if (surnameForm.value().replace(/\s/g, "") != "")
        return true;
    formService.SetError($("#surname"));
    alert("Vaya..el campo Apellido no es correcto");
    return false;
}


var mailForm = {};
mailForm.value = function () {
    return document.getElementById("mail").value;
}

mailForm.isValid = function () {
    if (mailForm.value().replace(/\s/g, "") != "" &&
        _validateEmail(mailForm.value()))
        return true;
    formService.SetError($("#mail"));
    alert("Vaya..el campo Mail no es correcto");
    return false;
}

var repeatMailForm = {};
repeatMailForm.value = function () {
    return document.getElementById("repeatMail").value;
}

repeatMailForm.isValid = function () {
    if (repeatMailForm.value().replace(/\s/g, "") != "" &&
        _validateEmail(repeatMailForm.value()))
        return true;
    formService.SetError($("#repeatMail"));
    alert("Vaya..el campo RepeatMail no es correcto");
    return false;
}

var dniForm = {};
dniForm.value = function () {
    return document.getElementById("dni").value;
}

dniForm.isValid = function () {
    if (dniForm.value().replace(/\s/g, "") != "" &&
        _validateNif(dniForm.value()))
        return true;
    formService.SetError($("#dni"));
    alert("Vaya..el campo Dni no es correcto");
    return false;
}

var mobileForm = {};
mobileForm.value = function () {
    return document.getElementById("mobile").value;
}

mobileForm.isValid = function () {
    if (mobileForm.value().replace(/\s/g, "") != "" &&
        _validateMobile(mobileForm.value())
    )
        return true;
    formService.SetError($("#mobile"));
    alert("Vaya..el campo Mobile no es correcto");
    return false;
}

// Generic functions //

formService.isValid = function () {
    let isValid = (nameForm.isValid() &&
        surnameForm.isValid() &&
        mailForm.isValid() &&
        repeatMailForm.isValid() &&
        dniForm.isValid() &&
        mobileForm.isValid());

    if (isValid && mailForm.value() == repeatMailForm.value()) {
        console.log("validate => TRUE");
        return true;
    }
    else if(isValid == true && mailForm.value() != repeatMailForm.value()){
        formService.SetError($("#repeatMail"));
        return false;
    }

    return false;
}

formService.GetContactData = function () {
    contact = {};
    contact.name = nameForm.value();
    contact.surname = surnameForm.value();
    contact.mail = mailForm.value();
    contact.repeatMail = repeatMailForm.value();
    contact.dni = dniForm.value();
    contact.mobile = mobileForm.value();

    return contact;
}

formService.clearContactData = function () {
    $("#name").val('');
    $("#surname").val('');
    $("#mail").val('');
    $("#repeatMail").val('');
    $("#dni").val('');
    $("#mobile").val('');
}

formService.SetError = function (form) {
    form.addClass("input-error");
}

formService.clearError = function (form) {
    form.removeClass("input-error");
}

// Subscribers //

$("#name").on('input', function () {
    formService.clearError($("#name"));
});

$("#surname").on('input', function () {
    formService.clearError($("#surname"));
});

$("#mail").on('input', function () {
    formService.clearError($("#mail"));
});

$("#repeatMail").on('input', function () {
    formService.clearError($("#repeatMail"));
});

$("#dni").on('input', function () {
    formService.clearError($("#dni"));
});

$("#mobile").on('input', function () {
    formService.clearError($("#mobile"));
});

// Validations //

function _validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function _validateNif(dni) {
    let numero
    let letr
    let letra
    let expresion_regular_dni

    expresion_regular_dni = /^\d{8}[a-zA-Z]$/;

    if (expresion_regular_dni.test(dni) == true) {
        numero = dni.substr(0, dni.length - 1);
        letr = dni.substr(dni.length - 1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero + 1);
        if (letra != letr.toUpperCase()) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function _validateMobile(mobile) {
    let haveAllNums = (mobile.length == 9)

    if (haveAllNums == true && mobile[0] == 6 || mobile[0] == 7)
        return true;
    else
        return false;

}